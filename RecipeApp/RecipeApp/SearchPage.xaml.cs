﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using System.Net.Http;
using ModernHttpClient;
using RecipeApp.Models;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using Plugin.Connectivity;

namespace RecipeApp
{
	public partial class SearchPage : ContentPage
	{
		public SearchPage ()
		{
			InitializeComponent ();
		}

        private static string MASHAPE_API_KEY = "l2l5W9MHPnmshpbErtbUArHBwEM8p1YP1HYjsnRwcCRbVhZwJR";

        async void Handle_Search(object sender, System.EventArgs e)
        {
            bool answer = true;
            while ((CrossConnectivity.Current.IsConnected == false) && answer == true)
            {
                answer = await DisplayAlert("Alert", "No internet connection", "Retry", "Cancel");
            }
            if (CrossConnectivity.Current.IsConnected == true)
            {
                LoadingIndicator.IsRunning = true;

                var client = new HttpClient(new NativeMessageHandler());

                string query = SearchText.Text.Replace(" ", "+");

                var uri = new Uri(string.Format("https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/search?number=5&offset=0&query=" + query));

                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                client.DefaultRequestHeaders.Add("X-Mashape-Key", MASHAPE_API_KEY);
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                HttpResponseMessage response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    ResultsItem recipe = JsonConvert.DeserializeObject<ResultsItem>(content);
                    ObservableCollection<RecipeList> recipeCollection = new ObservableCollection<RecipeList>();

                    for (int i = 0; i < recipe.total; i++)
                    {
                        recipeCollection.Add(recipe.result[i]);
                    }

                    LoadingIndicator.IsRunning = false;
                    ListViewRecipe.ItemsSource = recipeCollection;
                }
    
            }
        }

        async void Handle_ContextMenuMoreButton(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var recipe = (RecipeList)menuItem.CommandParameter;
            await Navigation.PushAsync(new MoreInfoPage(recipe));
        }

    }
}