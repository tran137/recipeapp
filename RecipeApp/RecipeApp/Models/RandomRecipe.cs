﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RecipeApp.Models
{
    public partial class RandomRecipe
    {
        [JsonProperty("recipes")]
        public List<Recipe> recipe { get; set; }
    }

    public partial class RandomRecipe
    {
        public static RandomRecipe FromJson(string json) => JsonConvert.DeserializeObject<RandomRecipe>(json, RecipeApp.Models.Converter.Settings);
    }
}
