﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RecipeApp.Models
{
    public partial class Recipe
    {
        [JsonProperty("title")]
        public String title { get; set; }

        [JsonProperty("readyInMinutes")]
        public String readyInMinutes { get; set; }

        [JsonProperty("image")]
        public String image { get; set; }

        [JsonProperty("instructions")]
        public String instructions { get; set; }

        [JsonProperty("extendedIngredients")]
        public List<Ingredient> ingredients { get; set; }

        [JsonProperty("servings")]
        public string servings { get; set; }


    }

    public partial class Recipe
    {
        public static Recipe FromJson(string json) => JsonConvert.DeserializeObject<Recipe>(json, RecipeApp.Models.Converter.Settings);
    }
}
