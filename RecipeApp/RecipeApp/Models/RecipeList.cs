﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RecipeApp.Models
{
    public partial class RecipeList
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("title")]
        public String title { get; set; }

        [JsonProperty("readyInMinutes")]
        public String readyInMinutes { get; set; }

        [JsonProperty("image")]
        public String image { get; set; }
    }
    
    public partial class RecipeList
    {
        public static RecipeList FromJson(string json) => JsonConvert.DeserializeObject<RecipeList>(json, RecipeApp.Models.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this RecipeList self) => JsonConvert.SerializeObject(self, RecipeApp.Models.Converter.Settings);
    }


    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            }
        };
    }
}
