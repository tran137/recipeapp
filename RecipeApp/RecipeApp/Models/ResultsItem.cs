﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RecipeApp.Models
{
    public partial class ResultsItem
    {
        [JsonProperty("results")]
        public List<RecipeList> result { get; set; }

        [JsonProperty("number")]
        public int total { get; set; }
    }

    public partial class ResultsItem
    {
        public static ResultsItem FromJson(string json) => JsonConvert.DeserializeObject<ResultsItem>(json, RecipeApp.Models.Converter.Settings);
    }
}
