﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RecipeApp.Models
{
    public partial class Ingredient
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("originalString")]
        public String originalString { get; set; }

        [JsonProperty("image")]
        public String image { get; set; }
    }

    public partial class Ingredient
    {
        public static Ingredient FromJson(string json) => JsonConvert.DeserializeObject<Ingredient>(json, RecipeApp.Models.Converter.Settings);
    }
}
