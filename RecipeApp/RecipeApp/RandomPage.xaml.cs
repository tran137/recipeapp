﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using System.Net.Http;
using ModernHttpClient;
using RecipeApp.Models;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using Plugin.Connectivity;

namespace RecipeApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RandomPage : ContentPage
	{
		public RandomPage ()
		{
			InitializeComponent ();
		}

        private static string MASHAPE_API_KEY = "API Key";

        async void GetRandomRecipe(object sender, System.EventArgs e)
        {
            bool answer = true;
            while ((CrossConnectivity.Current.IsConnected == false) && answer == true)
            {
                answer = await DisplayAlert("Alert", "No internet connection", "Retry", "Cancel");
            }
            if (CrossConnectivity.Current.IsConnected == true)
            {
                LoadingIndicator.IsRunning = true;

                var client = new HttpClient(new NativeMessageHandler());
                var uri = new Uri(string.Format("https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/random?number=1"));
                var request = new HttpRequestMessage();

                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                client.DefaultRequestHeaders.Add("X-Mashape-Key", MASHAPE_API_KEY);
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                HttpResponseMessage response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    RandomRecipe RandomRecipe = JsonConvert.DeserializeObject<RandomRecipe>(content);
                    Recipe random = RandomRecipe.recipe[0];

                    LoadingIndicator.IsRunning = false;

                    BindingContext = random;
                    IngredientList.Text = "Ingredients\n";
                    for (int i = 0; i < random.ingredients.Count; i++)
                    {
                        IngredientList.Text = IngredientList.Text + random.ingredients[i].originalString + "\n";
                    }
                }

            }
        }
    }
}