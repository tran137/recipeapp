﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using System.Net.Http;
using ModernHttpClient;
using RecipeApp.Models;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using Plugin.Connectivity;

namespace RecipeApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MoreInfoPage : ContentPage
    {
        public MoreInfoPage(RecipeList recipe)
        {
            InitializeComponent();
            GetRecipe(recipe);
        }

        private static string MASHAPE_API_KEY = "l2l5W9MHPnmshpbErtbUArHBwEM8p1YP1HYjsnRwcCRbVhZwJR";

        async void GetRecipe(RecipeList recipe)
        {
            if (CrossConnectivity.Current.IsConnected == false)
            {
                await DisplayAlert("Alert", "No internet connection", "OK");
            }
            else
            {
                var client = new HttpClient(new NativeMessageHandler());

                var uri = new Uri(string.Format("https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/" + recipe.id + "/information"));

                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                client.DefaultRequestHeaders.Add("X-Mashape-Key", MASHAPE_API_KEY);
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                HttpResponseMessage response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Recipe recipeDetailed = JsonConvert.DeserializeObject<Recipe>(content);
                    BindingContext = recipeDetailed;
                    IngredientList.Text = "Ingredients\n";
                    for (int i = 0; i < recipeDetailed.ingredients.Count; i++)
                    {
                        IngredientList.Text = IngredientList.Text + recipeDetailed.ingredients[i].originalString + "\n";
                    }
                }

            }
        }
    }
}
