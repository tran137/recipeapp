﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RecipeApp
{
	public partial class ConversionPage : ContentPage
	{
		public ConversionPage ()
		{
			InitializeComponent ();

            MeasurementType.SelectedIndex = 0;
        }

        private void typeChanged(object sender, EventArgs e)
        {
            var measurementList = new List<string>();

            if (MeasurementType.SelectedIndex == 0)
            {
                measurementList.Add("Pound");
                measurementList.Add("Ounce");
            }
            else
            {
                measurementList.Add("Quart");
                measurementList.Add("Pint");
                measurementList.Add("Cup");
                measurementList.Add("Ounce");
                measurementList.Add("Tablespoon");
                measurementList.Add("Teaspoon");
            }

            MeasurementInput.ItemsSource = measurementList;
            MeasurementOutput.ItemsSource = measurementList;

            MeasurementInput.SelectedIndex = 0;
            MeasurementOutput.SelectedIndex = 0;

            AmountOutput.Text = "";
        }

        private void convertClicked(object sender, EventArgs e)
        {
            double input;

            try
            {
                input = System.Convert.ToDouble(AmountInput.Text);
            }
            catch (FormatException)
            {
                AmountOutput.Text = "Error";
                return;
            }
            catch (OverflowException)
            {
                AmountOutput.Text = "Error";
                return;
            }

            if((String)MeasurementType.SelectedItem == "Mass")
            {
                if ((String)MeasurementInput.SelectedItem == (String)MeasurementOutput.SelectedItem)
                {
                    AmountOutput.Text = input.ToString("N3");
                }
                if (((String)MeasurementInput.SelectedItem == "Ounce") && ((String)MeasurementOutput.SelectedItem == "Pound"))
                {
                    input = input / 16;
                    AmountOutput.Text = input.ToString("N3");
                }
                if (((String)MeasurementInput.SelectedItem == "Pound") && ((String)MeasurementOutput.SelectedItem == "Ounce"))
                {
                    input = input * 16;
                    AmountOutput.Text = input.ToString("N3");
                }
            }
        }

        private void measurementChanged(object sender, EventArgs e)
        {
            AmountOutput.Text = "";
        }
    }
}